---
layout: post
title: El fenómeno de la web de mierda
author: Nick Heer

---

> Publicado originalmente como [The Bullshit
> Web](https://pxlnv.com/blog/bullshit-web/)

En 1998 mi computadora personal tenía un modem de 56k conectado
a nuestra línea telefónica. Se nos permitía un máximo de treinta minutos
de uso de la computadora al día, porque mis xadres --muy
razonablemente-- no querían tener el teléfono inutilizado durante toda
la tarde. Recuerdo que las páginas web cargaban lentamente: diez
a veinte segundos por un artículo básico de noticias.

Por ese entonces, algunes de mis amigues estaban empezando a contratar
Internet por cable. Resultaba notable ver las mismas páginas cargando en
algunos pocos segundos y recuerdo pensar en las posibilidades que se
abrirían con un web cada vez más rápida.

Y se hizo más rápida, por supuesto. Cuando me mudé a mi propio
departamento hace bastantes años atrás, puede elegir mi plan, una
conexión de banda ancha de 50 impresionantes megabits por segundo, la
cuál a estas alturas, ya aumenté aún más.

Entonces, con una conexión a Internet más rápida de lo que imaginé
posible a fines de los 90', ¿cómo quedó el puntaje? Una noticia en *The
Hill* demoró unos nueve segundos en cargar; en *Politico*, diecisiete
segundos; en *CNN*, casi treinta segundos. Este es el fenómeno de la web
de mierda.

Pero primero, un pequeño paréntesis: he estado escribiendo artículos
largos y cortos respecto a este asunto por un tiempo ya, pero quise
juntar varios hilos en un único documento que pueda ser pretensiosamente
descrito como una teoría de o, de manera más práctica, una guía sobre la
web de mierda.

Un segundo paréntesis: cuando uso la expresión "de mierda" en éste
artículo, no es en un sentido meramente profano. Es mucho más cercano a
la definición de Harry Frankfurt en su artículo "On Bullshit" \[Sobre la
mierda\]:

> Es tan solo la falta de conexión con cualquier precupación por la
> verdad --esta indiferencia por como las cosas son en realidad-- lo que
> considero la esencia de algo que es de mierda.

También pretendo utilizarlo en la misma forma que David Graeber en "[El
fenómeno de los trabajos de
mierda](/zines/el_fenomeno_de_los_laburos_de_mierda.html)":

> En el año 1930, John Maynard Keynes pronosticó que llegados a fin de
> siglo, la tecnología habría avanzado lo suficiente para que países
> como Gran Bretaña o Estados Unidos pudieran implementar una semana
> laboral de 15 horas. No faltan motivos para creer que tenía razón,
> dado que nuestra tecnología actual nos lo permitiría. Y sin embargo,
> no ha ocurrido. De hecho, la tecnología se ha encauzado, en todo caso,
> para inventar las condiciones para que todas trabajemos más. Para
> lograrlo se han creado trabajos que, en efecto, no tienen ningún
> sentido. Enormes cantidades de personas, especialmente en Europa y
> Estados Unidos, se pasan la totalidad de su vida laboral realizando
> tareas que, en el fondo, consideran totalmente innecesarias. Es una
> situación que provoca una herida moral y espiritual muy profunda. Es
> una cicatriz que marca nuestra alma colectiva. Pero casi nadie habla
> de ello.
>
> \[...\]
>
> Estos trabajos son lo que propongo denominar "laburos de mierda"

¿Cuál es entonces el equivalente en la Web?

1
=

La conexión a Internet promedio en los Estados Unidos es casi seis veces
más rápida que hace tan solo diez años atrás, pero en lugar de hacer que
el mismo tipo de contenido sea más rápido de navegar, simplemente
estamos ocupando el ancho de banda extra con más cosas. Algunas de estas
cosas son asombrosas: en el 2006, *Apple* añadió películas al *iTunes
Store* que tenían una resolución de 640 × 480 píxeles, pero ahora
podemos ver películas en resolución HD y (la pretensión de) 4K. Estas
altas velocidades nos permiten ver fotos mucho más detalladas y eso está
muy bien.

Pero muchas de las cosas que estamos viendo en casi todos los sitios
grandes son un montón de basura que no hace nada por la felicidad de sus
visitantes y que por el contrario, es profundamente irritante y
moralmente indefendible.

Tomemos aquel artículo de CNN como ejemplo. Ésto es lo que contiene al
cargar:

-   Once fuentes tipográficas, por un total de 414 KB[^bullshit1]

-   Cuatro hojas de estilo, por un total de 315 KB

-   Veinte marcos de contenido incorporado desde otras fuentes

-   Veintinueve peticiones HTTP de XML, por un total de unos 500 KB

-   Aproximadamente cien programas Javascript, por un total de varios
    Megabytes --resultando difícil saber con certeza su número y tamaño
    real ya que, técnicamente, algunos de estos cargan después de que la
    página terminó de descargar.

La vasta mayoría de estos recursos no están directamente relacionados
con la információn de la página, inclyendo la publicidad. Muchos de los
programas *Javascript* cargados son puramente para propósitos de
vigilancia: estadísticas internas, de las cuales hay muchos ejemplos;
muchas estadísticas de terceros, como *Salesforce*, *Chartbeat*, y
*Optimizely*; además de botones de difusión en medios sociales. Esto fuerza
la CPU y produce que mi computadora de seis años chille con dolor y
furia. No le estoy pidiendo demasiado; apenas le pedí que abriera un
documento de texto en la Web.

Sumado a eso, prácticamente cualquier artículo en *CNN* incluye algún
video que se reproduce automáticamente, una táctica que les ha permitido
alardear respecto a tener el número más alto de reproducciones de video
que cualquier otro medio. No tengo acceso a las estadísticas de *ComScore
Media Metrix*, así que no sé exactamente cuántas de aquellas millones de
reproducciones de video fueron detenidas instantáneamente, ya sea porque
le visitante presionó frenéticamente cada botón en el reproductor hasta
que el video desapareció, o simplemente cerró la pestaña del navegador
en su desesperación. Pero sospecho que será aproximadamente cada una de
ellas. La gente realmente odia los videos que se reproducen de manera
automática.

Además, ¿notaron cuántos sitios web intentan desesperadamente hacerte
registrar en su *newsletter*? Si bien esto es notorio en montones de
blogs y sitios de noticias, ya he arrastrado mucho el tema, así que
variaré un poco: también es una técnica super popular en sitios de
venta. Desde *Barnes & Noble* hasta *Aritzia*, *Fluevog* y *Linus
Bicycles*, estos formularios están por todas partes. Obtené un cupón
nominal a cambio de que te envíen un email que no leerás, cada día,
todos los días, para siempre --no, gracias.

Finalmente, hay un montón de elementos que se han vuelto una especie de
estándar en el diseño actual de sitios web que si bien no son
ofensivamente intrusos, a menudo son innecesarios. Siento un gran
aprecio por la buena tipografía, pero las fuentes tipográficas aún
cargan bastante lento y hacen que el texto cambie de tamaño cuando vas
por la mitad del primer párrafo. Y después vienen esas imágenes gigantes
de las cabeceras de ancho completo que dominan el encabezado de cada
página, como si cada artículo de doscientas palabras en un sitio de
noticias fuera equivalente al destacado de una revista.

Esa sería apenas la punta del iceberg de la web de mierda. ¿Sabían que
al construir carreteras más anchas no se mejoran los tiempos del
transporte, sino que simplemente se motiva a más gente a salir en auto?
Es lo mismo, pero con *bytes* y ancho de banda en lugar de automóviles y
calles.

2
=

Como Graeber observa en su ensayo y su libro, los trabajos de mierda
tienden a generar otros trabajos de mierda cuya única función es
depender de otros trabajos de mierda:

> Estas cifras ni siquiera reflejan a todas las personas que se dedican
> a proveer apoyo administrativo, técnico o de seguridad para esas
> industrias, por no mencionar toda la gama de sectores secundarios
> (cuidadoras de perros, repartidoras de pizza 24hs.) que tan solo deben
> su existencia a que el resto de la población pase tantísimo tiempo
> trabajando en otros sectores.

Este también es el caso de la web de mierda. La combinación de imágenes
enormes que no sirven otro propósito más que decorar, montones de
programas en *Javascript* que registran qué tan abajo llegaste en una
página y decenas más relacionados con publicidad, producen que simples
documentos de texto se vuelvan inaccesibles, hediendo a un odio casual
por les visitantes.

Si asumimos que cualquier ancho de banda adicional del que dispongan les
desarrolladores será inmediatamente utilizado, pareciera haber solo una
solución posible: reducir la cantidad de *bytes* que se transmiten. Por
alguna razón bizarra, esto no sucede en la web principal, porque de
cierta manera tiene más sentido crear una copia exacta de cada página en
el mismo sitio que esté expresamente diseñada para ser más veloz.
Bienvenido de vuelta WAP --excepto que, por alguna razón, esta copia
movilcentrica es completamente dependiente de todavía más bytes. Ésta es
la asombrosamente estúpida premisa detrás de AMP[^bullshit2].

Lanzado en Febrero de 2016, AMP es una colección de éstandares para
elementos HTML y otros propios, montados sobre una página especial,
ostensiblemnete más liviana, que necesita un archivo de 80 Kilobytes de
*JavaScript* para cargar correctamente. Permítanme explicar: HTML5 permite
elementos estructurales propios como el `<amp-img>` de AMP, pero los
presenta como elementos de tipo `<span>` sin ninguna instrucción
adicional --pero en el caso de AMP, este archivo *JavaScript* extra es
obligatorio. Este tremendo archivo además requiere, según la
especificación AMP, que se lo vincule desde el servidior
`cdn.amp-project.org`, que pertenece a *Google*. Lo cual hace que un
sitio dependiente de AMP sea a su vez dependiente de *Google* para poder
mostrar su etiquetado básico, lo cuál es super extraño para una
plataforma inherentemente abierta como la web.

Aquello desmiente el por qué AMP despegó en primer lugar. No es
necesariamente porque las paginas AMP sean mejores para les usuaries,
aunque eso se pueda considerar, sino porque *Google* quiere que sea
popular. Cuando buscás en *Google* cualquier cosa remotamente
relacionada con la actualidad, verás solamente páginas AMP en la lista
de noticias que aparece usualmente sobre los resultados de la búsqueda.
También es posible encontrar vínculos AMP copando la primera página de resultados.
*Google* ha admitido abiertamente que promueven las páginas AMP en sus
resultados de búsqueda y que el listado está restringido únicamente a
vínculos AMP en sus resultados para celulares. Insisten en que esto es
porque las páginas AMP son más rápidas y por ende, mejores para les
usuaries. Pero esta no es una explicación completa por tres razones:
las páginas AMP no son inherentemente más rápidas que las que no son
AMP, las páginas que posicionan bien y que no son AMP no se mezclan con
las AMP y además *Google* tiene un conflicto de intereses al promover el
formato.

Parece ridículo argumentar que las páginas AMP no son más veloces que
sus contrapartes en HTML estándar, porque resulta sencillo comprobar que
lo son. Pero hay una buena razón. No es que haya algún tipo de magia en
el formato AMP o alguna brillante mejora en su programación. La
diferencia es que AMP restringe el tipo de elementos que pueden ser
usados en una página y limita severamente la cantidad de programas
Javascript que pueden ser utilizados. Esto significa que las páginas web
no pueden ser rellenadas con basura arbitraria ni numerosos programas de
registro de uso y publicidad lo que, por supuesto, lleva a una página
dramáticamente más rápida. Una serie de experimentos conducidos por Tim
Kadlec demostró el efecto de estas limitaciones:

> La mayor ventaja de AMP no es el program --se puede lograr lo mismo
> por cuenta propia. Tampoco es la memoria de AMP --se puede lograr lo
> mismo con una buena reducción de código y todo aquello a través de un
> proveedor de contenido decente. Esto no significa que no haya
> elementos inteligentes lo que hace AMP, porque los hay. Pero no es
> esto lo que hace la mayor diferencia desde una perspectiva de
> rendimiento.
>
> La mayor ventaja de AMP es la restricción que pone sobre qué tantas
> cosas se pueden colocar en una sola página.
>
> \[...\]
>
> Las restricciones de AMP implica que las páginas contienen menos
> cosas. Es una concesión que los editores están dispuestos a hacer a
> cambio de la distribución ampliada que provee *Google*, pero que dudan
> en implementar en sus versiones oficiales.

Entonces: si tenemos un servidor razonablemente rápido y no rellenamos
las páginas con basura, también podemos tener resultados similares a los
de AMP sin crear una copia del sitio dependiente de *Google* y su
indexación lenta para retomar el control sobre la infraestructura de la
web. Pero no podemos tener acceso a las posiciones especiales reservadas
por *Google* para sitios AMP por razones que son ciertamente motivadas
por su interés propio.


3
=

Existe un efecto acumulativo de la mierda, su intensidad y su alcance
son especialmente profundos. Por sí solos, no resultan muchos los pocos
segundos que toma cargar un extra de *JavaScript* para la vigilancia.
Tampoco lo es el tiempo que nos demora como usuaries descartar una
ventana de suscripción a un *newsletter*, o pausar un video que se
reproduce automáticamente. Pero estas acciones se acumulan en una sola
página web y todavía más a través de multiples sitios web. Y esos
incrementos aparentemente pequeños en los tiempos de carga se convierten
en un miasma turbulento de dolor y frustración.

Es clave reconocer que esto es una decisión, una responsabilidad y --en
última instancia-- un asunto de respeto. Volvamos a la explicación de
Graeber sobre los trabajos de mierda y su observación de que a menudo
experimentamos semanas laborales de quince horas, mientras que pasamos
cuarenta en el lugar de trabajo. Mucho de aquello es cierto también en
la web: existe la capacidad de cargar páginas en un segundo o dos, pero
en cambio ha sido usada para espiar los hábitos de navegación de les
usuaries, hacernos miserables y acosarnos en los sitios que frecuentamos
y en nuestras casillas de correo electrónico.

En lo que respecta a la definición de Frankfurt --que la esencia de lo
que es de mierda es una indiferencia por la forma en que las cosas son
en realidad--, se manifiesta en el tratamiento selectivo de los
problemas reales de la web en favor de pseudo-soluciones deshonestas
como AMP.

Una solución real pasa por reconocer que toda esta mierda es
inexcusable. Es convertir la web, de manera acumulativa, en un lugar
horrible. Tras puertas cerradas, aquellos en la industria del marketing
y de la publicidad, pueden ser bastante lúcidos respecto a cuánto odian
como nosotres los programas de vigilancia y cuán horribles consideran
sus propios métodos, mientras simultáneamente promueven su uso. Por otro
lado, les usuaries tomamos las cosas en nuestras propias manos --el uso
de bloqueadores de avisos está en auge, muchos de los cuales además
bloquean programas de vigilancia y otros comportamientos irrespetuosos.
Les usuaries estamos haciendo esa elección.

No deberíamos tener que hacerlo. Les desarrolladores web deberían tomar
mejores decisiones para no publicar toda esta mierda en primer lugar. No
toleraríamos ese comportamiento intrusivo a un nivel más general; ¿por
qué esperamos que sea aceptable en la web?

Una web honesta es una en la cual la abrumadora mayoría del código y
recursos descargados a la computadora de une usuarie son usados en su
presentación visual, con casi todo el remanente siendo utilizado para
definir una estructura semántica y metadatos asociados en la página. La
mierda --ya sea en forma de vigilancia que sobrecarga la computadora,
elementos innecesariamente distrayentes y comportamientos que nadie
responsable por un sitio web consideraría sensato como visitante-- es
mal recibido e intolerable.

Muerte a la web de mierda.

[^bullshit1]: 1 Kilobyte equivale a 1024 bytes y es la unidad anterior
    a Megabyte.  En este caso, 400KB es más o menos el espacio que ocupa
    un documento de texto (nota de la edición).

[^bullshit2]: AMP es una tecnología de *Google* que obliga a los sitios
    a tener una copia de su contenido en una estructura especial para
    poder ser priorizados por la sección de noticias del buscador. WAP
    es una técnica similar para acceder a sitios web en dispositivos
    móviles no-inteligentes (nota de la edición).

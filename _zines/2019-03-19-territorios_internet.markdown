---
title: "¿Territorio internet?  Espacios, afectividades y comunidades"
author: la_jes
layout: post
cover: assets/covers/territorio_internet.png
contra: assets/covers/territorio_internet_contra.png
---


# ¿Territorio internet?  Espacios, afectividades y comunidades

La implementación de megaproyectos extractivos sigue causando daños en
los territorios y perpetuando la violencia hacia las comunidades que en
éstos habitan.  El presente artículo propone un acercamiento a Internet
como un espacio territorial, social y de creación, abordando en forma
breve su vinculación con temas ambientales, afectivos y de resistencia.

## La dimensión ambiental

Los minerales forman parte de nuestra cotidianidad, sin ellos la vida
tal y como la conocemos no sería posible.  Estos minerales también están
presentes en la sociedad de la información actual, la cual tiene un
fuerte componente tecnológico basado en infraestructuras informáticas
materiales.  La 'nube', por ejemplo, también es materia: suele decirse
que es la computadora de alguien más en algún otro lugar del mundo.  Y
aunque "la percepción generalizada sobre la tecnología considera que su
impacto medioambiental es escaso"[^1] ésta consume minerales, plásticos
y mucha energía.

Bajo el sistema capitalista actual la explotación de estos materiales se
produce a gran escala y tiene implicaciones que lo hacen insostenible
para la vida: legaliza el despojo territorial y patrimonial de los
lugares donde se asienta; propicia desarticulación de los tejidos
comunitarios y locales; contamina aguas, tierra y aire; provoca
enfermedades.  Las violaciones a los derechos humanos son constantes y
sistemáticas.  Una vez más el sistema privatiza los beneficios (que van
únicamente en dirección a las corporaciones y sus aledaños) y socializa
las pérdidas.  Las tecnologías tienden a reproducir esas asimetrías.

El impacto del consumo tecnológico podemos verlo tanto al final de la
linea de producción (por poner uno de los ejemplos más reconocidos, el
basurero electrónico de Ghana[^2]), como en medio, al constatar que
las industrias de las tecnologías de la información y la comunicación
son responsables de entre el 2% y el 3% de las emisiones de gases de
efecto invernadero en el mundo[^3].

En medio hay un largo proceso de producción que para el caso de los
teléfonos móviles por ejemplo, sabemos que tienen una vida de uso que en
promedio no supera los 18 meses.  Por ponerlo en cifras, para la
fabricación de cada smartphone se utilizan más de 200 minerales, 80
elementos químicos y más de 300 aleaciones y variedades de plástico.
Los minerales metálicos más utilizados son: cobre, estaño, cobalto,
níquel, coltán, oro y plata, muchos de ellos llamados 'minerales de
sangre' y su extracción se realiza en mayor medida en territorios del
Sur Global donde no solo hay disponibilidad de ellos sino condiciones
'favorables': escasas exigencias de seguridad, permisos ambientales
laxos, permisividad de trabajo infantil, presencia de grupos armados
'leales' a los capitales, etc.

Por otro lado no es posible tener datos certeros sobre consumo de
combustible necesario para la transportación de las materias primas a
los lugares de fundición y luego a los de fabricación de las piezas y de
éstos a los lugares donde se ensamblan (la más de las veces cada proceso
sucede en países diferentes) y de ahí a la cadena de transporte que
llevará los dispositivos a las vitrinas donde llegan relucientes y ya
sin las 'manchas' de antaño.  Las condiciones laborales en toda esta
cadena de producción (extracción, fabricación, ensamblaje,
transportación, venta) no son, ni mucho menos, diferentes a las de
cualquier maquila.  Incluyendo el hecho de que son las mujeres quienes
sufren las consecuencias con más frecuencia: "solo en Corea del Sur, la
organización Sharps ha documentado más de 370 casos de trabajadoras de
la industria electrónica, mujeres en su mayoría, que contrajeron cáncer
y enfermedades incurables con apenas 20 ó 30 años de edad"[^4].

En cuanto al uso, se dice que "la industria de las TIC estaría
consumiendo el 7% de toda la energía eléctrica generada a nivel
mundial"[^5], siendo que la gran mayoría de ese porcentaje (entre el 82%
y el 84%) sería consumo realizado por les usuaries finales.  Las cadenas
de mensajes, nuestras redes sociales, nuestras fotos de gatitos, no son
inmateriales.

## La dimensión afectiva

Escribo sobre este tema mirando la pantalla de mi computadora mientras
reviso los mensajes que van llegando...  ¿Para qué nos conectamos?  ¿Qué
hacemos cuando deambulamos por internet?  ¿A qué espacios estamos
accediendo?  Cuando nos preguntamos por qué 'estamos' en las redes
sociales comerciales o usamos al gran oráculo de las búsquedas solemos
respondernos que porque 'ahí está todo el mundo'.  Otras respuestas
suelen tener que ver con desconocer otros espacios donde realizar las
mismas actividades.

Sin embargo si miramos de cerca a esas mismas redes sociales
comerciales, en particular las de Zuckerberg (dueño de Facebook,
Instagram, Whatsapp) la respuesta tendría más que ver con que se
interesan en nuestra necesidad de conexión y comunicación y la usan para
explotarnos ¿silenciosamente?  Un 'arrepentido' ex alto cargo de la red
azulita contaba hace unos meses[^6] que para lograr consumir tanto
tiempo y atención consciente de nuestra parte como fuera posible,
encontraron que debían darnos ciertas dosis de 'dopamina' mediante 'me
gusta' y comentarios.  "Y eso te va a llevar a aportar más contenido...
Los inventores, los creadores --soy yo, es Mark \[Zuckerberg\], es Kevin
Systrom en Instagram, es toda esta gente-- lo entendimos
conscientemente".  Ellos decidieron que para 'echar a andar' un negocio
millonario explotarían una 'vulnerabilidad' de la psicología humana que
propiciaría la dependencia a sus plataformas 'sociales'.

Los software (programas informáticos) que hacen que las tecnologías
funcionen de cierta manera están escritas por personas (en su mayoría
hombres blancos heteros de los países desarrollados) que reproducen la
ideología de su 'clase' mientras se 'esconden' bajo el disfraz de la
neutralidad y la innovación.

Somos seres sociales y las redes los saben.  Son espacios 'digitales' que
propician nuestro estar en ellas de formas tales que queramos
interactuar.  Espacios de debate, compartición y apapachos.  Las redes
están diseñadas para comunicar personas (y/o grupos, colectivos,
instituciones) entre sí.  Sin embargo en las redes sociales más usadas
(Facebook, Instagram, Twitter) la mediación se da a través de burbujas
de filtro[^7], es decir, algoritmos que deciden qué vemos y qué no
vemos según criterios que imponen las propias empresas y a los cuales
las personas usuarias no tenemos acceso.

Nuestros datos personales, nuestra navegación es transparente para ellos
y recorre los caminos de sus infraestructuras hasta sus centros de datos
manejadas con algoritmos opacos que se niegan a compartir con nosotres
mientras nos dicen que 'la privacidad ya no existe' y que no debemos
preocuparnos si 'no tenemos nada que ocultar'[^8].  Y eso es justo lo
que debería suceder: porque no tenemos nada que esconder nadie debería
estar urgando constantemente.

Hay muchos motivos por los cuales podemos elegir enmascarar nuestras
identidades digitales.  Para las mujeres y personas no binarias un buen
motivo tiene que ver con la violencia en linea presente a diario en las
redes sociales.  Se multiplican discursos de odio, hostigamiento,
discriminación[^9], publicación de información e imágenes íntimas sin
el consentimiento de las personas protagonistas.  Es una violencia
'real' que trae consecuencias en la vida de las personas tanto a nivel
físico como emocional, "impacta la toma de decisiones sobre situaciones
cotidianas, como su forma de vestir y si salir o no de sus casas"[^10].

Pero Internet es más que redes sociales e incluso ahí donde no
pretendemos 'socializar', sino indagar, buscar, viajar, etc también
debemos enfrentarnos a dejar relegada nuestra privacidad en manos de más
burbujas de filtros.  Se suele decir que si el servicio es gratis el
producto sos vos.  Sabemos que en el capitalismo nada es gratis pero
confiamos plenamente en que las plataformas comerciales no tienen costo
para les usuaries.  El costo son nuestros datos e interacciones.

Alguien dijo alguna vez que necesitamos aprender a usar las tecnologías
tanto como hemos aprendido a leer y escribir[^11].  Defender internet
como territorio implica generar sociabilidades presentes, conscientes,
críticas.  Seguir usando la red, pero de formas que nos sean
beneficiosas, amorosas, compartidas sin seguir únicamente 'reglas'
impuestas por plataformas que no nos representan.  Defender Internet es
sumarnos a los grupos de "competentes literatos digitales que puedan
estar a la altura de las difíciles circunstancias que presumiblemente
nos tocará vivir"[^12].  Defender Internet como territorio nos ayuda a
tomar decisiones.

## A las afueras de la caja... de Facebook

Me dejo sonreír cada vez que alguien pide mi nombre para buscarme en
"redes sociales" (el plural es inútil, porque casi solo refieren 'al
feis') y ante mi negativa optan por preguntar mi teléfono para buscarme
en 'whats'.  La segunda negativa delimita el fin del interrogatorio y una
sentencia: ¿'no era que trabajabas con tecnología'?

Comencé a usar Facebook en 2011.  Las motivaciones fueron más bien
excusas y quizás por eso no era del todo localizable por ese medio,
pero ahí estaba.  Las idas y venidas por los caminos de las tecnologías
críticas (en un plural que implica experimentar en diversos estadíos,
softwares y plataformas tecnológicas) hicieron que me alejara cada vez
más de ella y finalmente sucedió.  Cerré mi cuenta, definitivamente, a
inicio de 2018.  El 'escándalo' de Cambridge Analytics[^13] me agarró ya
fuera de esa red social de la que llevaba meses borrando
sistemáticamente mi perfil para poder cerrarlo.

El proceso fue lento y complejo incluso para una persona como yo que
'casi no publicaba'.  La red social no quiere que la abandones, sin vos y
el contenido que generás, sería una plataforma vacía.  El tiempo que me
llevó este borrado sistemático me permitió volver a 'mirarme' a través
del espejo de lo que había querido mostrar allí.  Recordé momentos de
tristeza, satisfacción y enojo plasmados con palabras poco acertadas y
tiradas al aire, sin más, como si las interacciones que recibía tras
esas publicaciones 'llenaran' los vacíos que recordé haber sentido.

Hace un tiempo leía[^14] que "al igual que limpiar un armario, purgar
nuestras redes sociales puede no resultar sencillo... las personas
forman vínculos sentimentales con sus recuerdos en las redes sociales,
como lo hacen con la ropa y las fotografías antiguas".

Internet no olvida.  Facebook no me ha olvidado.  En algunos de los muchos
servidores que tiene desparramados por el mundo, donde replica varias
veces la información de cada une de sus usuaries, debo estar yo, en
algún 'perfil sombra'[^15] almacenado a la espera de ser mercantilizado
nuevamente.  Un 'shadow profile' es un archivo oculto de los datos
obtenidos a través de la información recopilada y entregada voluntaria e
involuntariamente a través de nuestras publicaciones, interacciones,
amistades, dispositivos, etcétera, para establecer patrones de uso[^16].

Esas tecnologías no están pensadas para que podamos jugar con ellas,
aprender de ellas, construir con ellas.  Sus reglas nos son impuestas y
ponen todos sus recursos a disposición de saber todo lo que decimos.
To-do.  Facebook no quiere que te vayas porque sin vos (literalmente) no
vale nada.

Fui yo la que decidió acabar con esa relación tóxica y salir a jugar.

## La dimensión de las resistencias.  Hacer red desde el hackfeminismo

Muchos son los contextos en los que transitamos las rutas de internet,
de ida y regreso, completas y por partes, cientos de veces al día.  Para
quienes además vemos este espacio como un territorio en si mismo \[y no
solo un medio para cumplir otros fines comunicativos\], internet es un
espacio público y político.

Para un grupo más reducido de personas lo político está dado por llevar
la potencia de hacer red a la Red, por habitar internet, por construirlo
como un espacio 'revoltoso'.

El acceso continúa siendo uno de los principales problemas de internet
hoy en día.  Sin embargo, el acceso no se limita únicamente a tener
conexión, hay grandes desigualdades que se dan apenas ingresamos a
internet: las brechas de uso, apropiación, transformación, creación de
sueños a través de internet son quizás mucho más grandes que las de la
propia conexión.  Para habitar internet es necesario conocerla, saber
sobre sus potencias y complicaciones, permitirnos maravillarnos por las
herramientas diversas que podemos conocer, las prácticas que podemos
generar, las personas detrás de la pantalla que hacen y deshacen más
allá de las redes comerciales que se muestran pomposas como punta de
iceberg.

En el marco del Primer Congreso Feminista de Chiapas realizado en
noviembre de 2016, la filósofa feminista Silvia Gil fue convocada a
comentar algunas de las aportaciones que los feminismos pueden hacernos
a la hora de \[re\]pensar las crisis sistémicas.  Habló de la necesidad
de encontrar formas de politización que toquen la vida y sacudan
nuestras cuerpas, de imaginar nuevos sentidos para proponernos
respuestas a las crisis y de buscar formas políticas que no den la
espalda a los cuidados, a la interdependencia de las cuerpas.
Escuchándola y pensando en las diversas dimensiones que conforman
internet, pensar en él como territorio a habitar cobra sentido
nuevamente.  Escobar[^17] define territorio como un espacio construido
por las interacciones sociales de quienes lo habitan, desde
organizaciones sociales y colectivos

En este campo los feminismos también tienen mucho que aportar.  Politizar
los procesos con formas y formatos imaginativos, de creación de nuevos
sentidos y narrativas.  Decía Gil que "para vivir necesitamos sostenernos
cotidianamente con trabajos materiales e inmateriales (afectos, deseos,
etc) que hacen posible la vida (...) No podemos aceptar una política que
dé la espalda al problema del cuidado, a la interdependencia de los
cuerpos, a la vulnerabilidad de la vida".  En este sentido, tener una
mirada crítica sobre las tecnologías que usamos a diario es un aporte
casi revolucionario en un contexto en el que las grandes empresas que
proveen servicios se muestra como mero ello: servicios únicos e
indispensables sin los cuales no existiría la Red.

Lo cierto es que en gran medida internet tal y como lo conocemos tiene
una fuerte dependencia de grandes infraestructuras y capitales.  Sin
embargo, son muchos los proyectos que cada vez más se levantan para
poner en cuestión esa máxima y aportar a reconstruir un ecosistema de
internet más biodiverso.  Redes de telecomunicaciones libres
comunitarias como GuifiNet[^18] en el Estado Español, Rizhomatica[^19]
en México o NetWork Bogotá[^20] en Colombia.  Servidores autónomos en
producción donde alojar nuestros contenidos y crear formas de trabajo
'sanas y cercanas' como las cooperativas tecnológicas MayFirst[^21] o
Kéfir[^22].  Redes sociales distribuidas donde organizarnos,
interactuar, reconocernos, encontrarnos como Mastodon[^23] o
Diáspora[^24].  Hay más ejemplos que son más que simples ejemplos.

Habitar las tecnologías hoy es mucho más que acceder a plataformas
comerciales: existen ya opciones múltiples que nos permiten gestionar
los recursos de nuestras computadoras libremente[^25], resolver nuestras
necesidades de búsqueda de información sin ser rastreadas[^26], trabajar
colaborativamente[^27], almacenar información en bibliotecas
digitales[^28] creadas para sumar al bien común.

Podemos dar los primeros pasos cuestionando los sistemas de comunicación
actuales, aprendiendo sobre ellos, su funcionamiento, sus opciones y
propuestas.  Reconciliarnos con el hecho de que los pasitos digitales
que damos sí son importantes.  Las imágenes que compartimos, los lugares
físicos que transitamos, las búsquedas que realizamos, las cosas que nos
gustan son la fuente de mercantilización actual de las plataformas
comerciales.  Ellas entendieron a la perfección que la información que
brindamos tiene valor.  Y como en el capitalismo valor solo es igual a
dinero, nos monetizan, nos venden al mejor postor de la publicidad
comercial[^29] o política[^30], total 'no tenemos nada que
ocultar'[^31].

¿Cómo ser antisistema[^32] en medio de tecnologías de comunicación
eminentemente sistémicas?

Cuando podemos mirar más allá del brillo tecnológico con el que las
multinacionales tecnológicas pretenden deslumbrarnos podemos comenzar a
ver la red de interdependencia que subyace al desarrollo tecnológico.  Y
ese es el principio de poder construir tecnologías más justas, cercanas,
propias y afectivas que resuelvan problemas reales.  Existe en nuestra
inteligencia colectiva la capacidad radical de construir cada aspecto de
nuestras vidas con términos propios, con otros imaginarios y
potencialidades; "si iniciamos por cuestionar la concepción misma de
tecnología y la despojamos de su supuesta neutralidad y objetividad,
podemos encontrar un camino para darnos cuenta de cómo la tecnología se
convierte en una forma de estar y relacionarse con el mundo
culturalmente construida y cuáles son las relaciones de poder que la
hacen parte de las redes de los discursos sociales"[^33].

Dado el primer paso del cuestionamiento llega la potencia que bien
conocemos desde los feminismos, la de aprender con otres, compartir
conocimiento.  Podemos defender internet como territorio que habitamos
[^34]: y que podemos transformar.  No se trata solo de 'estar' allí,
sino de hacerlo desde la complejidad de cuestionar sus entrañas y gozar
los espacios digitales con colegas cómplices con les que aprendamos,
discutamos y nos 'encontremos' desde la escucha.

Las propuestas hackfeministas[^35] actuales proponen desde allí.  Para
mí la apropiación tecnológica o mejor aún, la adopción tecnológica, es
ante todo, feminista.  No decidirán sobre nuestras cuerpas, tampoco
sobre nuestres seres ciborgs, redes sociales y espacios digitales que
habitamos.  Para poder transformar las relaciones de poder actuales
también en los espacios digitales necesitamos permitirnos 'deconstruir'
nuestra relación con las tecnologías, permitirnos tener 'habitaciones
digitales propias', encontrarnos con otras en aquelarres
tecnofeministas[^36] en los que podamos compartir nuestras experiencias
afectivas, de lucha y de satisfacción, permitirnos jugar, aprender,
romper y equivocarnos.  Y seguir haciendo desde fuera-de-la-caja[^37].


Publicado originalmente en
<https://sursiendo.com/blog/2019/03/territorio-internet-espacios-afectividades-y-comunidades/>


[^1]: Revista Chasqui.  "La cara oculta de la sociedad de la
  información: el impacto medioambiental de la producción, el consumo y
  los residuos tecnológicos", dirección URL:
  <http://www.revistachasqui.org/index.php/chasqui/article/view/3321/2975>
  (página consultada en septiembre de 2018).

[^2]: Red Voltaire. "África, el basurero del mundo", dirección URL:
  <https://www.voltairenet.org/article186749.html> (página consultada en
  septiembre de 2018).

[^3]: Ensayos de Economía.  "Las tecnologías de la información y el
  cambio climático en los países en desarrollo", dirección URL:
  <http://bdigital.unal.edu.co/35507/1/35857-143873-1-PB.pdf> (página
  consultada en septiembre de 2018).

[^4]: Revista Chasqui.  "La cara oculta de la sociedad de la
  información: el impacto medioambiental de la producción, el consumo y
  los residuos tecnológicos", dirección URL:
  <http://www.revistachasqui.org/index.php/chasqui/article/view/3321/2975>
  (página consultada en septiembre de 2018).

[^5]: Idem

[^6]: Genbeta.  "Sean Parker, primer presidente de Facebook: la red
  social explota una "vulnerabilidad" humana", dirección URL:
  <https://www.genbeta.com/redes-sociales-y-comunidades/sean-parker-primer-presidente-de-facebook-la-red-social-explota-una-vulnerabilidad-humana>
  (página consultada en septiembre de 2018).

[^7]: TED Talks.  "Eli Pariser: Beware online 'filter bubbles'",
  dirección URL:
  <https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles>
  (página consultada en septiembre de 2018).

[^8]: TEDxMadrid.  "¿Por qué me vigilan si no soy nadie?", dirección
  URL: <https://www.hooktube.com/watch?v=NPE7i8wuupk> (página
  consultada en septiembre de 2018).

[^9]: Prensa Latina.  "Acusan a Facebook en EE.UU. de anuncios que
  discriminan a las mujeres", dirección URL:
  <https://www.prensa-latina.cu/index.php?o=rn&id=211423&SEO=acusan-a-facebook-en-ee.uu.-de-anuncios-que-discriminan-a-las-mujeres>
  (página consultada en septiembre de 2018).

[^10]: Internet es Nuestra MX.  "#FalsaProtección Cuatro errores que se
  deben evitar al combatir la violencia en línea", dirección URL:
  <http://internetesnuestra.mx/post/158075258118/falsaproteccio-n-cuatro-errores-que-se-deben>
  (página consultada en septiembre de 2018).

[^11]: Sursiendo, Comunicación y Cultura Digital.  "Software libre más
  allá de la libertad", dirección URL:
  <https://sursiendo.com/blog/2014/06/software-libre-mas-alla-de-la-libertad/>
  (página consultada en septiembre de 2018).

[^12]: CCCB Lab, Investigación e Innovación en Cultura.  "Una revolución
  educativa", dirección URL:
  <http://lab.cccb.org/es/una-revolucion-educativa/> (página consultada
  en septiembre de 2018).

[^13]: Letras Libres.  "Cambridge Analytics y los alcances de la ciencia
  de datos", dirección URL:
  <https://www.letraslibres.com/mexico/ciencia-y-tecnologia/cambridge-analytica-y-los-alcances-la-ciencia-datos>
  (página consultada en septiembre de 2018).

[^14]: Revista Yorokobu.  "¿Podrías hacer limpieza en tu pasado
  digital?", dirección URL: <https://www.yorokobu.es/purgar/> (página
  consultada en septiembre de 2018).

[^15]: Radio Canadá Internacional.  "Perfiles sombra de Facebook y como
  la red social te sigue hasta cuando has borrado tu cuenta", dirección
  URL:
  <http://www.rcinet.ca/es/2018/04/14/perfiles-sombra-de-facebook-y-como-la-red-social-te-sigue-hasta-cuando-has-borrado-tu-cuenta/>
  (página consultada en septiembre de 2018).

[^16]: Genbeta, "Una prueba demuestra que Facebook comparte tu "perfil
  sombra" con anunciantes", dirección URL:
  <https://www.genbeta.com/redes-sociales-y-comunidades/prueba-demuestra-que-facebook-comparte-tu-perfil-sombra-anunciantes>
  (página consultada en septiembre de 2018).

[^17]: Escobar, Arturo (2010).  Territorios de diferencia.  Lugar
  movimientos vida redes.  Universidad de Carolina del Norte, Chapel
  Hill: Envión Ediciones.

[^18]: <https://guifi.net/es>

[^19]: <https://www.rhizomatica.org/>

[^20]: <https://networkbogota.org/>

[^21]: <https://mayfirst.org/es/>

[^22]: <https://kefir.red/>

[^23]: <https://joinmastodon.org/>

[^24]: <https://joindiaspora.com/>

[^25]: Sursiendo, Comunicación y Cultura Digital.  "¿Listas para
  decidir?: ¡el software libre es para vos!", dirección URL:
  <https://sursiendo.com/blog/2016/05/lista-para-decidir-el-software-libre-es-para-vos/>
  (página consultada en septiembre de 2018).

[^26]: <https://duckduckgo.com/>

[^27]: <https://nextcloud.com/>

[^28]: <https://archive.org/>

[^29]: El Salto Diario.  "Las trampas de la publicidad en Internet",
  dirección URL:
  <https://www.elsaltodiario.com/consumo-que-suma/las-trampas-de-la-publicidad-en-internet>
  (página consultada en septiembre de 2018).

[^30]: Denken Über.  "Facebook y Cambridge Analytica: solo un síntoma de
  un problema más grande", dirección URL:
  <http://www.uberbin.net/archivos/derechos/facebook-y-cambridge-analytica-solo-un-sintoma-de-un-problema-mas-grande.php>
  (página consultada en septiembre de 2018).

[^31]: Jérémie Zimmermann et la Parisienne Libérée.  "Rien à cacher",
  dirección URL: <https://www.hooktube.com/watch?v=rEwf4sDgxHo> (página
  consultada en septiembre de 2018).

[^32]: Sursiendo, Comunicación y Cultura Digital.  "Incidir es también
  hackear el hacer", dirección URL:
  <https://sursiendo.com/blog/2015/08/incidir-es-tambien-hackear-el-hacer/>
  (página consultada en septiembre de 2018).

[^33]:
  <http://www.elsalmon.co/2017/04/trayectoria-socio-tecnica-de-las.html>

[^34]: Sursiendo, Comunicación y Cultura Digital.  "El desafío de una
  internet feminista: la reconfiguración necesaria", dirección URL:
  <https://sursiendo.com/blog/2014/04/el-desafio-de-una-tecnologia-feminista-la-reconfiguracion-necesaria/>
  (página consultada en septiembre de 2018).

[^35]: Radio Irratia.  "Podcast del programa radial Mar de Fueguitos con
  entrevista a Sursiendo", dirección URL:
  <https://archive.org/details/mardefueguitos-con-entrevista-la_jes>
  (página consultada en septiembre de 2018).

[^36]: Sursiendo, Comunicación y Cultura Digital.  "Cruzar líneas:
  tecnologías, género y activismos", dirección URL:
  <https://sursiendo.com/blog/2015/01/cruzar-lineas-tecnologias-genero-y-activismos/>
  (página consultada en septiembre de 2018).

[^37]: Dossier Ritimo.  "Soberanía Tecnológica", dirección URL:
  <http://www.plateforme-echange.org/IMG/pdf/dossier-st-cast-2014-06-30.pdf>
